<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('assets/') ?>img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?= base_url('assets/') ?>img/favicon.png">
    <title>
        <?= $title; ?>
    </title>
    <!--     Fonts and icons     -->
    <link href="<?= base_url('assets/') ?>https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="<?= base_url('assets/') ?>css/nucleo-icons.css" rel="stylesheet" />
    <link href="<?= base_url('assets/') ?>css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="<?= base_url('assets/') ?>https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="<?= base_url('assets/') ?>css/nucleo-svg.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="<?= base_url('assets/') ?>css/soft-ui-dashboard.css?v=1.0.3" rel="stylesheet" />
</head>

<body class="g-sidenav-show  bg-gray-100">
    <section class="min-vh-100 mb-8">
        <div class="page-header align-items-start min-vh-50 pt-5 pb-11 m-3 border-radius-lg" style="background-image: url('../assets/img/curved-images/curved14.jpg');">
            <span class="mask bg-gradient-dark opacity-6"></span>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5 text-center mx-auto">
                        <h1 class="text-white mb-2 mt-5">Welcome!</h1>
                        <p class="text-lead text-white">Create your Account!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row mt-lg-n10 mt-md-n11 mt-n10">
                <div class="col-xl-4 col-lg-5 col-md-7 mx-auto">
                    <div class="card z-index-0">
                        <div class="row px-xl-5 px-sm-4 px-3">
                            <div class="mt-2 position-relative text-center">
                            </div>
                        </div>
                        <div class="card-body">
                            <form class="user" method="post" action="<?= base_url('auth/registration'); ?>">
                                <form role="form text-left">
                                    <div class="mb-3">
                                        <input type="text" class="form-control form-control-user" id="email" name="email" placeholder="Email" value="<?= set_value('email') ?>">
                                        <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control form-control-user" id="nisn" name="nisn" placeholder="Nisn">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control form-control-user" id="nis" name="nis" placeholder="Nis">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control form-control-user" id="nama" name="nama" placeholder="Nama" value="<?= set_value('nama') ?>">
                                        <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <div class=" mb-3">
                                        <input type="text" class="form-control form-control-user" id="id_kelas" name="id_kelas" placeholder="Kelas">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control form-control-user" id="alamat" name="alamat" placeholder="Alamat">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control form-control-user" id="no_telp" name="no_telp" placeholder="No Telephone">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control form-control-user" id="id_spp" name="id_spp" placeholder="ID SPP">
                                    </div>
                                    <div class="mb-3">
                                        <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Password">
                                        <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-dark w-100 my-4 mb-2">Sign up</button>
                                    </div>
                                    <p class="text-sm mt-3 mb-0">Already have an account? <a href="<?= base_url(); ?>auth" class="text-dark font-weight-bolder">Sign in</a></p>
                                </form>
                                </from>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</body>