 
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->

    <h1 class="h3 mb-4 text-gray-800"></h1>
            <div class="row">
                <div class="col-lg-6">

                <a href="" class="btn btn-info mb-3" data-toggle="modal" data-target="#newSppModal">Edit Siswa</a>

                <?= form_error('datasiswa/read', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                 <?= $this->session->flashdata('message'); ?>

            </div>
         </div>
    </div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- MODAL -->

    <!-- Modal -->
    <div class="modal fade" id="newSppModal" tabindex="-1" aria-labelledby="newSppModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="newSppModalLabel">Edit Siswa</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="<?= base_url('datasiswa/update'); ?>" method="post">
        <div class="modal-body">

        <?php $row = $query->result(); ?>
             
           <div class="form-group">
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" value="<?= $row{0}->nama ?>">
            </div>
              <div class="form-group">
                  <input type="text" class="form-control" id="nisn" name="nisn" placeholder="Nisn" value="<?= $row{0}->nisn ?>">
            </div>

            <div class="form-group">
                  <input type="text" class="form-control" id="nis" name="nis" placeholder="Nis" value="<?= $row{0}->nis ?>">
            </div>
            <div class="form-group">
                  <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat" value="<?= $row{0}->alamat ?>">
            </div>
            <div class="form-group">
                  <input type="text" class="form-control" id="no_telp" name="no_telp" placeholder="No Telp" value="<?= $row{0}->no_telp ?>">
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Edit</button>
        </div>
        </form>
        </div>
    </div>
    </div>