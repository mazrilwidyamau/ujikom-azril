<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>



    <div class="row">
        <div class="col-lg-10">
            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>') ?>

            <a href="" class="btn btn-info mb-3" data-toggle="modal" data-target="#exampleModal">Tambah Siswa Baru</a>

            <?= $this->session->flashdata('message'); ?>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Siswa</th>
                        <th scope="col">Nisn</th>
                        <th scope="col">Nis</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">No Telp</th>

                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($semua_siswa as $s) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $s['nama']; ?></td>
                            <td><?= $s['nisn']; ?></td>
                            <td><?= $s['nis']; ?></td>
                            <td><?= $s['alamat']; ?></td>
                            <td><?= $s['no_telp']; ?></td>
                            <td>
                            <a href="<?= base_url('datasiswa/delete/') . $s['nisn']; ?>"><div class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></div>    
                            <a href="<?= base_url('datasiswa/read/') . $s['nisn']; ?>"><div class="btn btn-info btn-sm"><i class="fa fa-edit"></i></div> 
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Siswa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('datasiswa'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Siswa">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="nis" name="nis" placeholder="NIS">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="no_telp" name="no_telp" placeholder="No Telp">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-info">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>