<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>



    <div class="row">
        <div class="col-lg-8">
            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>') ?>

            <a href="" class="btn btn-info mb-3" data-toggle="modal" data-target="#exampleModal">Tambah Pembayaran</a>

            <?= $this->session->flashdata('message'); ?>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No</th>


                        <th scope="col">Nisn</th>
                        <th scope="col">Tanggal Bayar</th>
                        <th scope="col">Bulan Dibayar</th>
                        <th scope="col">Tahun Dibayar</th>

                        <th scope="col">Jumlah Bayar</th>

                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($pembayaran as $p) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $p['nisn']; ?></td>
                            <td><?= $p['tgl_bayar']; ?></td>
                            <td><?= $p['bulan_dibayar']; ?></td>
                            <td><?= $p['tahun_dibayar']; ?></td>
                            <td><?= $p['jumlah_bayar']; ?></td>
                            <td>

                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Pembayaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('pembayaran'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN">
                    </div>
                    <div class="form-group">
                        <input type="date" class="form-control" id="tgl_bayar" name="tgl_bayar" placeholder="Tanggal Bayar">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="bulan_dibayar" name="bulan_dibayar" placeholder="Bulan Dibayar">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="tahun_dibayar" name="tahun_dibayar" placeholder="Tahun Dibayar">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="jumlah_bayar" name="jumlah_bayar" placeholder="Jumlah Bayar">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-info">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>