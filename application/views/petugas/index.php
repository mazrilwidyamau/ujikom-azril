<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>



    <div class="row">
        <div class="col-lg-8">
            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>') ?>

            <a href="" class="btn btn-info mb-3" data-toggle="modal" data-target="#exampleModal">Tambah Petugas</a>

            <?= $this->session->flashdata('message'); ?>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No</th>

                        <th scope="col">Username</th>

                        <th scope="col">Nama Petugas</th>
                        <th scope="col">Level</th>

                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($semua_siswa as $p) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>

                            <td><?= $p['username']; ?></td>

                            <td><?= $p['nama_petugas']; ?></td>
                            <td><?= $p['level']; ?></td>
                            <td>
                            <a href="<?= base_url('petugas/delete/') . $p['id_petugas']; ?>"><div class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></div>    
                            <a href="<?= base_url('petugas/read/') . $p['id_petugas']; ?>"><div class="btn btn-info btn-sm"><i class="fa fa-edit"></i></div> 
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Petugas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('petugas'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="nama_petugas" name="nama_petugas" placeholder="Nama Petugas">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="password" name="password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <select class="form-select" id="level" name="level" placeholder="Level">
                            <option selected>Pilih Level</option>
                            <option value="petugas">Petugas</option>
                            <option value="admin">Admin</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-info">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>