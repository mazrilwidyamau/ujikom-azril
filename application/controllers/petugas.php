<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Petugas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('petugas_model');

    }
    public function index()
    {
        $data['title'] = "Petugas";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['semua_siswa'] = $this->db->get('petugas')->result_array();

        $this->form_validation->set_rules('username', 'username', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('petugas/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('petugas', ['username' => $this->input->post('username'), 'password' => $this->input->post('password'), 'nama_petugas' => $this->input->post('nama_petugas'), 'level' => $this->input->post('level')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Menu Added!</div>');
            redirect('petugas/index');
        }
    }
    public function create()
    {
        $data['title'] = "Petugas";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['semua_siswa'] = $this->db->get('petugas')->result_array();

        $this->form_validation->set_rules('username', 'username', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('petugas/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('petugas', ['username' => $this->input->post('username'), 'password' => $this->input->post('password'), 'nama_petugas' => $this->input->post('nama_petugas'), 'level' => $this->input->post('level')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Menu Added!</div>');
            redirect('petugas/index');
        }
    }
    public function delete($id_petugas)
{
    $this->petugas_model->delete($id_petugas);
    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Data spp telah dihapus!!</div>');
    redirect('petugas/index');
}
public function read($id_petugas)
{
    $data['title'] = 'Petugas';
    $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();


    $data['query'] = $this->petugas_model->read_by_id($id_petugas);
    $data['petugas'] = $this->db->get('petugas')->result_array();

    $this->form_validation->set_rules('username', 'username', 'required');
     $this->form_validation->set_rules('nama_petugas', 'nama_petugas', 'required');
     $this->form_validation->set_rules('level', 'level', 'required');


    if($this->form_validation->run() == false) {

     $this->load->view('templates/header', $data);
     $this->load->view('templates/sidebar', $data);
     $this->load->view('templates/topbar', $data);
     $this->load->view('petugas/read', $data);
     $this->load->view('templates/footer');

    } else {

         $data = array(
             'username'   =>   $this->input->post('username'),
             'nama_petugas'   =>   $this->input->post('nama_petugas'),
             'level' => $this->input->post('level'),
         
      );

         $this->db->insert('petugas', $data);
         $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Spp Added</div>');
          redirect('petugas/index');
    }
    
}
public function update()
{
    $username = $this->input->post('username');
    $nama_petugas = $this->input->post('level');
    $level = $this->input->post('level');

    $data['query'] = $this->petugas_model->update();
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Edit New Petugas Succes</div>');
    redirect('petugas/index');
}

    public function submenu()
    {
        $data['title'] = "Submenu Management";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'icon', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/submenu', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'title' => $this->input->post('title'),
                'menu_id' => $this->input->post('menu_id'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active')
            ];
            $this->db->insert('user_sub_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Submenu Added!</div>');
            redirect('menu/submenu');
        }
    }
}
