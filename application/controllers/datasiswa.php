<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Datasiswa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('siswa_model');

    }
    public function index()
    {
        $data['title'] = "Siswa";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['semua_siswa'] = $this->db->get('siswa')->result_array();

        $this->form_validation->set_rules('nisn', 'nisn', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('datasiswa/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('siswa', ['nisn' => $this->input->post('nisn'), 'nis' => $this->input->post('nis'), 'nama' => $this->input->post('nama'), 'alamat' => $this->input->post('alamat'), 'no_telp' => $this->input->post('no_telp')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Menu Added!</div>');
            redirect('datasiswa/index');
        }
    }
    public function create()
    {
        $data['title'] = "Siswa";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['semua_siswa'] = $this->db->get('siswa')->result_array();

        $this->form_validation->set_rules('nisn', 'nisn', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('kelas/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('datasiswa', ['nisn' => $this->input->post('nisn'), 'nis' => $this->input->post('nis'), 'nama' => $this->input->post('nama'), 'alamat' => $this->input->post('alamat'), 'no_telp' => $this->input->post('no_telp')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Menu Added!</div>');
            redirect('datasiswa/index');
        }
    }
    public function delete($nisn)
{
    $this->siswa_model->delete($nisn);
    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Data siswa telah dihapus!!</div>');
    redirect('datasiswa/index');
}

public function read($nisn)
{
    $data['title'] = 'Siswa';
    $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();


    $data['query'] = $this->siswa_model->read_by_id($nisn);
   

    $this->form_validation->set_rules('nama', 'nama', 'required');
     $this->form_validation->set_rules('nisn', 'nisn', 'required');
    $this->form_validation->set_rules('nis', 'nis', 'required');
     $this->form_validation->set_rules('alamat', 'alamat', 'required');


    if($this->form_validation->run() == false) {

     $this->load->view('templates/header', $data);
     $this->load->view('templates/sidebar', $data);
     $this->load->view('templates/topbar', $data);
     $this->load->view('datasiswa/read', $data);
     $this->load->view('templates/footer');

    } else {

         $data = array(
             'nama'   =>   $this->input->post('nama'),
             'nisn'   =>   $this->input->post('nisn'),
             'nis' => $this->input->post('nis'),
             'alamat' => $this->input->post('alamat'),
             'no_telp' => $this->input->post('no_telp'),
         
      );

         $this->db->insert('siswa', $data);
         $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Siswa Added</div>');
          redirect('datasiswa/index');
    }
    
}
public function update()
{
    $nama = $this->input->post('nama');
    $nisn = $this->input->post('nisn');
    $nis = $this->input->post('nis');
    $alamat = $this->input->post('alamat');
    $no_telp = $this->input->post('no_telp');

    $data['query'] = $this->siswa_model->update();
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Edit New Siswa Succes</div>');
    redirect('datasiswa/index');
}

    public function submenu()
    {
        $data['title'] = "Submenu Management";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'icon', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/submenu', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'title' => $this->input->post('title'),
                'menu_id' => $this->input->post('menu_id'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active')
            ];
            $this->db->insert('user_sub_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Submenu Added!</div>');
            redirect('menu/submenu');
        }
    }
}
