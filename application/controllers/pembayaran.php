<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $data['title'] = "Pembayaran";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['pembayaran'] = $this->db->get('pembayaran')->result_array();

        $this->form_validation->set_rules('nisn', 'nisn', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('pembayaran/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('pembayaran', ['nisn' => $this->input->post('nisn'), 'tgl_bayar' => $this->input->post('tgl_bayar'), 'bulan_dibayar' => $this->input->post('bulan_dibayar'), 'tahun_dibayar' => $this->input->post('tahun_dibayar'), 'jumlah_bayar' => $this->input->post('jumlah_bayar')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Menu Added!</div>');
            redirect('pembayaran/index');
        }
    }
    public function create()
    {
        $data['title'] = "Pembayaran";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['pembayaran'] = $this->db->get('pembayaran')->result_array();

        $this->form_validation->set_rules('nisn', 'nisn', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('pembayaran/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('pembayaran', ['nisn' => $this->input->post('nisn'), 'tgl_bayar' => $this->input->post('tgl_bayar'), 'bulan_dibayar' => $this->input->post('bulan_dibayar'), 'tahun_dibayar' => $this->input->post('tahun_dibayar'), 'jumlah_bayar' => $this->input->post('jumlah_bayar')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Menu Added!</div>');
            redirect('pembayaran/index');
        }
    }
    public function update()
    {
        $data['title'] = "Pembayaran";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['pembayaran'] = $this->db->get('pembayaran')->result_array();

        $this->form_validation->set_rules('nisn', 'nisn', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('pembayaran/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('pembayaran', ['nisn' => $this->input->post('nisn'), 'tgl_bayar' => $this->input->post('tgl_bayar'), 'bulan_dibayar' => $this->input->post('bulan_dibayar'), 'tahun_dibayar' => $this->input->post('tahun_dibayar'), 'jumlah_bayar' => $this->input->post('jumlah_bayar')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Menu Added!</div>');
            redirect('pembayaran/index');
        }
    }
    public function delete($pembayaran)
    {
        $this->pembayaranmodel->delete($pembayaran);
        redirect('pembayaran/index');
    }

    public function submenu()
    {
        $data['title'] = "Submenu Management";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'icon', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/submenu', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'title' => $this->input->post('title'),
                'menu_id' => $this->input->post('menu_id'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active')
            ];
            $this->db->insert('user_sub_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Submenu Added!</div>');
            redirect('menu/submenu');
        }
    }
}
