<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Spp extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Spp_model');

    }
    public function index()
    {
        $data['title'] = "SPP";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['semua_siswa'] = $this->db->get('spp')->result_array();

        $this->form_validation->set_rules('tahun', 'tahun', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('spp/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('spp', ['tahun' => $this->input->post('tahun'), 'nominal' => $this->input->post('nominal')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Menu Added!</div>');
            redirect('spp/index');
        }
    }
    public function create()
    {
        $data['title'] = "SPP";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['semua_siswa'] = $this->db->get('spp')->result_array();

        $this->form_validation->set_rules('tahun', 'tahun', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('spp/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('spp', ['tahun' => $this->input->post('tahun'), 'nominal' => $this->input->post('nominal')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Menu Added!</div>');
            redirect('spp/index');
        }
    }
     
public function delete($tahun)
{
    $this->Spp_model->delete($tahun);
    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Data spp telah dihapus!!</div>');
    redirect('spp');
}

public function read($id_spp)
{
    $data['title'] = 'Edit Spp';
    $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();


    $data['query'] = $this->Spp_model->read_by_id($id_spp);
    $data['spp'] = $this->db->get('spp')->result_array();

    $this->form_validation->set_rules('tahun', 'Tahun', 'required');
     $this->form_validation->set_rules('nominal', 'Nominal', 'required');


    if($this->form_validation->run() == false) {

     $this->load->view('templates/header', $data);
     $this->load->view('templates/sidebar', $data);
     $this->load->view('templates/topbar', $data);
     $this->load->view('spp/read', $data);
     $this->load->view('templates/footer');

    } else {

         $data = array(
             'id_spp'   =>   $this->input->post('id_spp'),
             'tahun'   =>   $this->input->post('tahun'),
             'nominal' => $this->input->post('nominal'),
         
      );

         $this->db->insert('spp', $data);
         $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Spp Added</div>');
          redirect('spp/index');
    }
    
}

public function update()
{
    $id_spp = $this->input->post('id_spp');
    $tahun = $this->input->post('tahun');
    $nominal = $this->input->post('nominal');

    $data['query'] = $this->Spp_model->update();
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Edit New SPP Succes</div>');
    redirect('spp/index');
}

    public function submenu()
    {
        $data['title'] = "Submenu Management";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'icon', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/submenu', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'title' => $this->input->post('title'),
                'menu_id' => $this->input->post('menu_id'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active')
            ];
            $this->db->insert('user_sub_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Submenu Added!</div>');
            redirect('menu/submenu');
        }
    }
}
