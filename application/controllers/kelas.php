<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('kelas_model');

    }
    public function index()
    {
        $data['title'] = "Kelas";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['semua_kelas'] = $this->db->get('kelas')->result_array();

        $this->form_validation->set_rules('nama_kelas', 'nama_kelas', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('kelas/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('kelas', ['nama_kelas' => $this->input->post('nama_kelas'), 'kompetensi_keahlian' => $this->input->post('kompetensi_keahlian')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Kelas Added!</div>');
            redirect('kelas/index');
        }
    }
    public function create()
    {
        $data['title'] = "Siswa";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['semua_siswa'] = $this->db->get('siswa')->result_array();

        $this->form_validation->set_rules('nama_siswa', 'nama_siswa', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('kelas/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('kelas', ['nama_kelas' => $this->input->post('nama_kelas'), 'kompetensi_keahlian' => $this->input->post('kompetensi_keahlian')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Menu Added!</div>');
            redirect('kelas/index');
        }
    }
    
    public function delete($id_kelas)
    {
        $this->kelas_model->delete($id_kelas);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Data spp telah dihapus!!</div>');
        redirect('kelas');
    }
    
    public function read($id_kelas)
{
    $data['title'] = 'Kelas';
    $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();


    $data['query'] = $this->kelas_model->read_by_id($id_kelas);
    $data['kelas'] = $this->db->get('kelas')->result_array();

    $this->form_validation->set_rules('nama_kelas', 'nama_kelas', 'required');
     $this->form_validation->set_rules('kompetensi_keahlian', 'kompetensi_keahlian', 'required');


    if($this->form_validation->run() == false) {

     $this->load->view('templates/header', $data);
     $this->load->view('templates/sidebar', $data);
     $this->load->view('templates/topbar', $data);
     $this->load->view('kelas/read', $data);
     $this->load->view('templates/footer');

    } else {

         $data = array(
             'id_kelas'   =>   $this->input->post('id_kelas'),
             'nama_kelas'   =>   $this->input->post('nama_kelas'),
             'kompetensi_keahlian' => $this->input->post('kompetensi_keahlian'),
         
      );

         $this->db->insert('kelas', $data);
         $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Spp Added</div>');
          redirect('kelas/index');
    }
    
}

public function update()
{
    
    $id_kelas = $this->input->post('id_kelas');
    $nama_kelas = $this->input->post('nama_kelas');
    $kompetensi_keahlian = $this->input->post('kompetensi_keahlian');

    $data['query'] = $this->kelas_model->update();
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Edit New Kelas Succes</div>');
    redirect('kelas/index');
}

    public function submenu()
    {
        $data['title'] = "Submenu Management";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'icon', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/submenu', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'title' => $this->input->post('title'),
                'menu_id' => $this->input->post('menu_id'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active')
            ];
            $this->db->insert('user_sub_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New Submenu Added!</div>');
            redirect('menu/submenu');
        }
    }
    
    
}
