<?php

class kelas_model extends CI_Model
{
    public function delete($id_kelas)
    {
        $this->db->delete('kelas', array('id_kelas' => $id_kelas)); // Produces: //DELETE FROM mytable // WHERE id = $id
    }

    public function read_by_id($id_kelas)
    {
        $query = $this->db->get_where('kelas', array('id_kelas' => $id_kelas));
        return $query;
    }

    public function update()
    {
        $data = array(

            'nama_kelas' => $this->input->post('nama_kelas'),
            'kompetensi_keahlian' => $this->input->post('kompetensi_keahlian'),           
        );

        $this->db->update('kelas', $data, array('id_kelas' => $this->input->post('id_kelas')));
    }
}