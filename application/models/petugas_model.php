<?php

class petugas_model extends CI_Model
{
    public function delete($id_petugas)
    {
        $this->db->delete('petugas', array('id_petugas' => $id_petugas)); // Produces: //DELETE FROM mytable // WHERE id = $id
    }

    public function read_by_id($id_petugas)
    {
        $query = $this->db->get_where('petugas', array('id_petugas' => $id_petugas));
        return $query;
    }

    public function update()
    {
        $data = array(

            'username' => $this->input->post('username'),
            'nama_petugas' => $this->input->post('nama_petugas'),           
            'level' => $this->input->post('level'),           
        );

        $this->db->update('petugas', $data, array('id_petugas' => $this->input->post('id_petugas')));
    }
}