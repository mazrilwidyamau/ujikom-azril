<?php

class siswa_model extends CI_Model
{
    public function delete($nisn)
    {
        $this->db->delete('siswa', array('nisn' => $nisn)); // Produces: //DELETE FROM mytable // WHERE id = $id
    }

    public function read_by_id($nisn)
    {
        $query = $this->db->get_where('siswa', array('nisn' => $nisn));
        return $query;
    }

    public function update()
    {
        $data = array(

            'nama' => $this->input->post('nama'),
            'nisn' => $this->input->post('nisn'),
            'nis' => $this->input->post('nis'),           
            'alamat' => $this->input->post('alamat'),           
            'no_telp' => $this->input->post('no_telp'),           
        );

        $this->db->update('siswa', $data, array('nama' => $this->input->post('nama')));
    }
}